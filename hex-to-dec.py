#!/usr/bin/env python
# coding: utf-8

# In[206]:


hexNumbers = {
    '0':0, '1':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, 
    'A':10, 'B':11, 'C':12, 'D':13, 'E':14, 'F':15 
}

def hexToDec(hexNum):
    decimal = 0
    i = len(hexNum)-1
    for j in hexNum:
        if j in hexNumbers:
            decimal=decimal+(hexNumbers[j]*16**i)
            i = i-1
        else:
            return None
    print(decimal)
    return
    
        
    

