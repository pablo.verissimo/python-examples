#!/usr/bin/env python
# coding: utf-8
from collections import defaultdict

##returns a compacted list of tuples from the received string 
def encodeString(stringVal):
    
    encodeDict = defaultdict(list)
    for item in stringVal: 
        if item not in encodeDict:
            i = 1
            encodeDict[item].append(i)
        else: 
            i = i+1
            encodeDict[item].append(i)
            
    keys = list(encodeDict.keys())
    values = list(encodeDict.values())

    codedString = []
    tuplas = ()
    while keys:
        tuplas = (str(keys.pop(0)),max(values.pop(0)))
        codedString = codedString + [(tuplas)]
    return (codedString) 

##returns a compacted string from the received string 
def encodeString2(stringVal):
    
    encodeDict = defaultdict(list)
    for item in stringVal: 
        if item not in encodeDict:
            i = 1
            encodeDict[item].append(i)
        else: 
            i = i+1
            encodeDict[item].append(i)
            
    keys = list(encodeDict.keys())
    values = list(encodeDict.values())

    codedString = ''
    while keys:
        codedString = codedString + str(keys.pop(0))
        codedString = codedString + str(max(values.pop(0)))
    return (codedString)
    

##returns an uncompressed string from the received compacted list of tuples
def decodeString(encodedList):
        
    decodedString = ''
    item = []
    
    for item in encodedList:
        decodedString = decodedString + str(item[0]*item[1])
    return (decodedString)




